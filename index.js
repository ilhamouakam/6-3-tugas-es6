// //Soal 1==============================================
let keliling=(P,L) => {
    return 2*P+2*L;
}
console.log(keliling(5,4))

//Soal 2===================================================
const newFunction1=(firstName, lastName)=>{
    console.log(firstName + " " + lastName)    
}

//Driver Code 
newFunction1("William", "Imoh")
  
//Soal 3==================================================
const newObject = {
    firstName: "Muhammad",
    lastName: "Iqbal Mubarok",
    address: "Jalan Ranamanyar",
    hobby: "playing football"
  }

const {firstName,lastName,address,hobby} = newObject

  // Driver code
console.log(firstName, lastName, address, hobby)


//Soal 4================================================
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
//before   const combined = west.concat(east)
const combined =[...west, ...east]

//Driver Code
console.log(combined)


//Soal 5 ===================================================
const planet = "earth" 
const view = "glass" 
//var before = 'Lorem ' + view + 'dolor sit amet, ' + 'consectetur adipiscing elit,' + planet 
let after = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet}`

console.log(after)

